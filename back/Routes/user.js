let express = require('express')
let router = express.Router()
let useCtrl = require('../Controllers/user')

router.post('/signup', useCtrl.signup)
router.post('/login', useCtrl.login)

module.exports = router