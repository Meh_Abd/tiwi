let express = require('express')
let tiwiCtrl = require('../Controllers/tiwi')
let auth = require('../middleware/auth')
let router = express.Router()

router.post('/addTiwi', auth, tiwiCtrl.addTiwi)
router.get('/userTiwis', tiwiCtrl.userTiwis)
router.post('/deleteTiwi', tiwiCtrl.deleteTiwi)

module.exports = router