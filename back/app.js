const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const userRoutes = require('./Routes/user')
const tiwiRoutes = require('./Routes/tiwi')
const cors = require('cors')

const app = express()

mongoose.connect('mongodb+srv://admin:admin@mycluster.lcqnb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
    useNewUrlParser: true})
    .then(() => console.log('Connecté à la Db !'))
    .catch((e) => console.log('Echec de la connexion à la Db !')
)

app.use(cors())

app.use(bodyParser.json())

app.use('/api/auth', userRoutes)
app.use('/api/auth', tiwiRoutes)

module.exports = app