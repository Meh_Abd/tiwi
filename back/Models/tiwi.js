let mongoose = require('mongoose')
let uniqueValidator = require('mongoose-unique-validator')

let tiwiSchema = mongoose.Schema({
    date: { type: String, required: true },
    user: { type: String, required: true },
    description: { type: String }
})

tiwiSchema.plugin(uniqueValidator)

module.exports = mongoose.model('Tiwi', tiwiSchema)