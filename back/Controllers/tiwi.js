let tiwis = require('../Models/tiwi')

exports.addTiwi = (req, res, next) => {
    let newTiwi = new tiwis({
        date: req.body.date,
        user: req.body.user,
        description: req.body.description
    })
    newTiwi.save()
    .then(() => res.status(200).json({ message: "Tiwi ajouté à la base" }))
    .catch(error => res.status(400).json({ error }))
}

exports.userTiwis = (req, res, next) => {
    tiwis.find()
    .then(usrTiwis => res.status(200).json(usrTiwis))
    .catch(error => res.status(400).json({ error }))
}

exports.deleteTiwi = (req, res, next) => {
    tiwis.deleteOne({ _id : req.body._id })
    .then(deleted => res.status(200).json({ message: "Tiwi supprimé" }))
    .catch(error => res.status(400).json({ error }))
}