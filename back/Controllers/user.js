let users = require('../Models/user')
let jwt = require('jsonwebtoken')

exports.signup = (req, res, next) => {
    let newUser = new users({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    })
    newUser.save()
        .then(() => res.status(200).json({ message: 'Utilisateur créé' }))
        .catch(error => res.status(400).json({ error }))
}

exports.login = (req, res, next) => {
    users.findOne({ email: req.body.email })
        .then(newUser => {
            if (!newUser) {
                return res.status(401).json({ error: 'user non trouvé' })
            }
            if (req.body.password === newUser.password) {
                res.status(200).json({
                    username: newUser.username,
                    userId: newUser._id,
                    token: jwt.sign(
                        { userId :newUser._id },
                        'TEMP_TOKEN',
                        {expiresIn: '24h'}
                    )
                })
            }
            else {
                res.status(401).json({ error })
            }
        })
        .catch(error => res.status(500).json({ error: 'mot de passe incorrecte' }))
}